// const url = 'src/students_data.json';
// const table = document.querySelector('table');
// fetch(url)
// 	.then((response) => response.json())
// 	.then((data) => {
// 		data.forEach((element) => {
// 			if (element.id <= 15) {
// 				table.innerHTML += `
//                <tr>
//                   <th> ${element.id}</th>
//                   <td class=""><img src=${element.photo} class="rounded-circle shadow-4 border-danger bg-danger"
//                   alt="Avatar" /> </td>
//                   <td> ${element.first_name}</td>
//                   <td> ${element.last_name}</td>
//                   <td> ${element.email}</td>
//                   <td> ${element.gender}</td>
//                </tr>
//             `;
// 			}
// 		});
// 	})
// 	.catch((error) => console.error('failes connection ..'));

const url = 'https://jsonplaceholder.typicode.com/posts';
const table = document.querySelector('table');
const request = new XMLHttpRequest();
const syncs = true;
const method = 'GET';
request.open(method, url, syncs);
request.onload = () => {
	let data = JSON.parse(request.responseText);
	//console.log(data);
	data.forEach((element) => {
		table.innerHTML += `
               <tr>
                  <td> ${element.id}</td>
                  <td> ${element.title}</td>
                  <td> ${element.body}</td>
               </tr>
            `;
	});
};
request.send();
