var timer = () => {
	let time = new Date();
	let hours = (time.getHours() % 12).toString();
	let minutes = time.getMinutes().toString();
	let seconds = time.getSeconds().toString();
	// let result = document.querySelector('h1');

	// result.innerHTML = hours + '  :  ' + minutes + '   :  ' + seconds;
	if (hours.length < 2) {
		hours = '0' + hours;
	}
	if (minutes.length < 2) {
		minutes = '0' + minutes;
	}
	if (seconds.length < 2) {
		seconds = '0' + seconds;
	}

	let result = document.querySelector('h1');
	if (time.getHours() < 12) {
		result.innerHTML = hours + '  :  ' + minutes + '  :  ' + seconds + ' AM';
	} else {
		result.innerHTML = hours + '  :  ' + minutes + '  :  ' + seconds + ' PM';
	}

	if (time.getHours() === 12) {
		result.innerHTML =
			time.getHours() + '  :  ' + minutes + '  :  ' + seconds + ' AM';
	}
};
setInterval(timer, 1000);
